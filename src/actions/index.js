import fetch from 'isomorphic-fetch';
import { SELECT_SUBREDDIT, INVALIDATE_SUBREDDIT, REQUEST_POSTS, RECEIVE_POSTS } from '../constants/actionTypes';

// sync action creators
export const selectSubreddit = (subreddit) => ({ type: SELECT_SUBREDDIT, subreddit });
export const invalidateSubreddit = (subreddit) => ({ type: INVALIDATE_SUBREDDIT, subreddit });
const requestPosts = (subreddit) => ({ type: REQUEST_POSTS, subreddit });
const receivePosts = (subreddit, json) => ({
    type: RECEIVE_POSTS,
    subreddit,
    posts: json.data.children.map(({ data }) => data),
    receivedAt: Date.now()
});

// async action creators
const shouldFetchPosts = (state, subreddit) => {
    const posts = state.postsBySubreddit[subreddit];
    return !posts
        ? true
        : posts.isFetching ? false : posts.didInvalidate;
};
export const fetchPosts = (subreddit) => (dispatch) => {
    dispatch(requestPosts(subreddit));
    return fetch(`http://www.reddit.com/r/${subreddit}.json`)
        .then((response) => response.json())
        .then((json) => dispatch(receivePosts(subreddit, json)));
};
export const fetchPostsIfNeeded = (subreddit) => (dispatch, getState) =>
    shouldFetchPosts(getState(), subreddit)
        ? dispatch(fetchPosts(subreddit))
        : Promise.resolve();
